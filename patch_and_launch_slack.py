""" Patch slack to have a dark mode, if it needs to be patched. """

import os
import sys
import subprocess
import time
import requests
from pprint import pprint
import getpass


SLACK_INSTALL_LOCATION = os.path.join("C:\\","Users",getpass.getuser(),"AppData","Local","slack")

# https://github.com/Nockiro/slack-black-theme
# Get the dark mode code from github.  They don't have a file that contains the code,
# so we have to parse it out of the readme.md file.

readme_request = requests.get('https://raw.githubusercontent.com/Nockiro/slack-black-theme/master/readme.md')
readme = readme_request.text.split("\n")
START_LINE = 0
END_LINE = 0
for number, line in enumerate(readme):
    if line== '```js' and not START_LINE:
        START_LINE = number+1
    if line == '```' and START_LINE:
        END_LINE = number
        break

if not START_LINE or not END_LINE:
    print("Could not find the code we need.")
    sys.exit(1)

print("Starts at {} and ends at {}".format(START_LINE, END_LINE))

SLACK_DARK_MODE = "\n\n// <SLACKDARKMODE>\n" + "\n".join(readme[START_LINE:END_LINE]) + "\n// </SLACKDARKMODE>\n"

APP_FOLDER = ''

directories = os.listdir(SLACK_INSTALL_LOCATION)
directories.reverse()

# Locate the slack app-* directory.
for contents in directories:
    if contents[0:4] == 'app-':
        APP_FOLDER = contents
        break
else:
    print("Could not find the app directory.")
    sys.exit(1)

INTEROP_LOCATION = os.path.join(SLACK_INSTALL_LOCATION, APP_FOLDER, 'resources','app.asar.unpacked','src','static','ssb-interop.js')
print(INTEROP_LOCATION)
ADDED_DARK_MODE = False

with open(INTEROP_LOCATION, "r+") as f:
    data = f.read()
    if SLACK_DARK_MODE not in data:
        print("We need to add dark mode.")
        f.seek(0, 2)
        f.write(SLACK_DARK_MODE)
        print("Dark mode added.")
        ADDED_DARK_MODE = True
    else:
        print("Did not need to patch Slack.")

if ADDED_DARK_MODE:
    print("Killing Slack, if it's already running.")
    os.system("taskkill /F /im slack.exe")
    time.sleep(5)
    print("Starting Slack")
    os.chdir(os.path.join(SLACK_INSTALL_LOCATION, APP_FOLDER))
    os.startfile(os.path.join(SLACK_INSTALL_LOCATION, 'slack.exe'))
